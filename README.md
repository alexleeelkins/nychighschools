Tech approach you took and why:
Since this app requires fetching data from a third party service, I first looked to the third party service to see what said data looks like.
Without knowing the structure of the data, it is nearly impossible to write any model-related code.
Once I knew what the data would look like, I wrote up some Codable-conforming models, a mockable network client, some networking client unit tests to validate that our network client is functional, and some services to utilize this client.
I am a big believer in the protocol / delegate approach for these types of calls as we can do all of the work in the background and coordinate updating the UI when that work is done.
Plus, using a delegate / service approach lets us move the model logic out of the ViewController and into its own space.

What was important to you — look and feel or the functionality?:
In my opinion, functionality is the most important. An app can look incredible, but if it isn’t useful then why would a user download it in the first place?
Look and feel isn’t worth anything if the app doesn’t work.
However, once we get the app functional, look and feel does matter.
Looking for places to reduce friction for the user are essential, also, like making the phone number clickable to call the phone number.
One other thing that could be done is making the address clickable to open Apple Maps.
These are things that people have come to expect. Reducing friction for the user will make them enjoy using the app.