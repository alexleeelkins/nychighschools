//
//  SATResult.swift
//  NYCHighSchools
//
//  Created by Alex Elkins on 7/19/19.
//  Copyright © 2019 Alex Elkins. All rights reserved.
//

import Foundation

struct SATResult: Codable {
    var dbn: String
    var numberOfTestTakers: String
    var criticalReadingAverageScore: String
    var mathAverageScore: String
    var writingAverageScore: String
    
    private enum CodingKeys: String, CodingKey {
        case dbn,
        numberOfTestTakers = "num_of_sat_test_takers",
        criticalReadingAverageScore = "sat_critical_reading_avg_score",
        mathAverageScore = "sat_math_avg_score",
        writingAverageScore = "sat_writing_avg_score"
    }
}
