//
//  SchoolModel.swift
//  NYCHighSchools
//
//  Created by Alex Elkins on 7/19/19.
//  Copyright © 2019 Alex Elkins. All rights reserved.
//

import Foundation

struct School: Codable {
    var dbn: String
    var schoolName: String
    var phoneNumber: String
    var addressLine1: String
    var city: String
    var state: String
    var zip: String
    
    private enum CodingKeys: String, CodingKey {
        case dbn,
        schoolName = "school_name",
        phoneNumber = "phone_number",
        addressLine1 = "primary_address_line_1",
        city,
        state = "state_code",
        zip
    }
}
