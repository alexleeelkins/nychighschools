//
//  NetworkService.swift
//  NYCHighSchools
//
//  Created by Alex Elkins on 7/19/19.
//  Copyright © 2019 Alex Elkins. All rights reserved.
//

import Foundation

class NetworkClient {
    // API endpoint for all schools' data
    var schoolDataEndpoint = "https://data.cityofnewyork.us/resource/s3k6-pzi2.json"
    // API endpoint for all schools' SAT results
    var satDataEndpoint = "https://data.cityofnewyork.us/resource/f9bf-2cp4.json"
    
    private let session: URLSessionProtocol
    
    init(session: URLSessionProtocol) {
        self.session = session
    }
    
    func get(url: URL, completion: @escaping (_ data: Data?, _ error: Error?) -> Void) {
        var request = URLRequest(url: url)
        request.httpMethod = "GET"
        let task = session.dataTask(with: request) { (data, response, error) in
            completion(data, error)
        }
        task.resume()
    }
}

extension URLSession: URLSessionProtocol {
    func dataTask(with request: URLRequest, completionHandler: @escaping URLSessionProtocol.DataTaskResult) -> URLSessionDataTaskProtocol {
        return dataTask(with: request, completionHandler: completionHandler) as URLSessionDataTask
    }
}

extension URLSessionDataTask: URLSessionDataTaskProtocol {}
