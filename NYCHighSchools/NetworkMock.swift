//
//  NetworkMock.swift
//  NYCHighSchools
//
//  Created by Alex Elkins on 7/19/19.
//  Copyright © 2019 Alex Elkins. All rights reserved.
//

import Foundation

class URLSessionMock: URLSessionProtocol {
    var nextDataTask = URLSessionMockDataTask()
    var nextData: Data?
    var nextError: Error?
    
    private (set) var lastUrl: URL?
    
    func successResponse(request: URLRequest) -> URLResponse {
        return HTTPURLResponse(url: request.url!, statusCode: 200, httpVersion: "HTTP/1.1", headerFields: nil)!
    }
    
    func dataTask(with request: URLRequest, completionHandler: @escaping DataTaskResult) -> URLSessionDataTaskProtocol {
        lastUrl = request.url
        completionHandler(nextData, successResponse(request: request), nextError)
        return nextDataTask
    }
}

class URLSessionMockDataTask: URLSessionDataTaskProtocol {
    private (set) var resumeWasCalled = false
    
    func resume() {
        resumeWasCalled = true
    }
}

protocol URLSessionProtocol {
    typealias DataTaskResult = (Data?, URLResponse?, Error?) -> Void
    
    func dataTask(with request: URLRequest, completionHandler: @escaping DataTaskResult) -> URLSessionDataTaskProtocol
}

protocol URLSessionDataTaskProtocol {
    func resume()
}
