//
//  SchoolDataService.swift
//  NYCHighSchools
//
//  Created by Alex Elkins on 7/19/19.
//  Copyright © 2019 Alex Elkins. All rights reserved.
//

import Foundation

class SchoolDataService {
    var schools: [School]? {
        didSet {
            self.schoolDataServiceDelegate.schoolsWereUpdated(schools: schools)
        }
    }
    
    var schoolDataServiceDelegate: SchoolDataServiceDelegate
    let session: URLSessionProtocol
    
    init(schoolDataServiceDelegate delegate: SchoolDataServiceDelegate, urlSession session: URLSessionProtocol = URLSession.shared) {
        self.schoolDataServiceDelegate = delegate
        self.session = session
    }
    
    func getSchools() {
        let networkClient = NetworkClient(session: self.session)
        guard let url = URL(string: networkClient.schoolDataEndpoint) else {
            fatalError("Failed to generate URL from High School data endpoint")
        }
        networkClient.get(url: url) { (data, error) in
            if error != nil {
                self.schoolDataServiceDelegate.failedToGetSchools()
                return
            }
            if let data = data {
                do {
                    let decoder = JSONDecoder()
                    let schools = try decoder.decode([School].self, from: data).sorted(by: { (lhsSchool, rhsSchool) -> Bool in
                        return lhsSchool.schoolName < rhsSchool.schoolName
                    })
                    self.schools = schools
                } catch {
                    fatalError("High School data endpoint provided severely invalid data!")
                }
            }
        }
    }
}
