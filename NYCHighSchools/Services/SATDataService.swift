//
//  SATDataService.swift
//  NYCHighSchools
//
//  Created by Alex Elkins on 7/19/19.
//  Copyright © 2019 Alex Elkins. All rights reserved.
//

import Foundation

class SATDataService {
    var satResult: SATResult? {
        didSet {
            self.satDataServiceDelegate.satResultWasUpdated(satResult: satResult)
        }
    }
    
    var satDataServiceDelegate: SATDataServiceDelegate
    let session: URLSessionProtocol
    
    init(satDataServiceDelegate delegate: SATDataServiceDelegate, urlSession session: URLSessionProtocol = URLSession.shared) {
        self.satDataServiceDelegate = delegate
        self.session = session
    }
    
    func getSatResultFor(school: School) {
        let networkClient = NetworkClient(session: self.session)
        guard let url = URL(string: networkClient.satDataEndpoint) else {
            fatalError("Failed to generate URL from SAT data endpoint")
        }
        networkClient.get(url: url) { (data, error) in
            if error != nil {
                assertionFailure("SAT data endpoint access failed!")
            }
            if let data = data {
                do {
                    let decoder = JSONDecoder()
                    let results = try decoder.decode([SATResult].self, from: data)
                    self.satResult = results.filter({ (satResult) -> Bool in
                        return satResult.dbn == school.dbn
                    }).first
                } catch {
                    fatalError("SAT data endpoint provided severely invalid data!")
                }
            }
        }
    }
}
