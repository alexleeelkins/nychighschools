//
//  SchoolDataServiceDelegate.swift
//  NYCHighSchools
//
//  Created by Alex Elkins on 7/19/19.
//  Copyright © 2019 Alex Elkins. All rights reserved.
//

import Foundation

protocol SchoolDataServiceDelegate {
    func schoolsWereUpdated(schools: [School]?)
    func failedToGetSchools()
}
