//
//  SchoolsViewController.swift
//  NYCHighSchools
//
//  Created by Alex Elkins on 7/19/19.
//  Copyright © 2019 Alex Elkins. All rights reserved.
//

import UIKit

class SchoolsViewController: UITableViewController {
    var schoolDataService: SchoolDataService?
    var schools: [School]? {
        didSet {
            self.tableView.reloadData()
            self.refreshControl?.endRefreshing()
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        refreshControl = UIRefreshControl()
        tableView.refreshControl = refreshControl
        refreshControl?.addTarget(self, action: #selector(getSchools), for: .valueChanged)
        schoolDataService = SchoolDataService(schoolDataServiceDelegate: self)
        getSchools()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.navigationBar.prefersLargeTitles = true
    }
    
    @objc func getSchools() {
        schoolDataService?.getSchools()
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return schools?.count ?? 0
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "SchoolCell", for: indexPath)
        guard let schools = self.schools, schools.indices.contains(indexPath.row) else {
            fatalError("Tried to access a school outside of the range of schools we retrieved!")
        }
        let school = schools[indexPath.row]
        cell.textLabel?.text = school.schoolName
        return cell
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "ShowSchoolDetailSegue" {
            guard let schools = self.schools, let selectedRow = tableView.indexPathForSelectedRow, schools.indices.contains(selectedRow.row) else {
                fatalError("Tried to access a school outside of the range of schools we retrieved!")
            }
            let school = schools[selectedRow.row]
            if let schoolDetailViewController = segue.destination as? SchoolDetailViewController {
                schoolDetailViewController.school = school
            }
        }
    }
}

extension SchoolsViewController: SchoolDataServiceDelegate {
    func failedToGetSchools() {
        DispatchQueue.main.async {
            self.refreshControl?.endRefreshing()
            let alert = UIAlertController(title: "Uh-oh!", message: "There was an error getting school data! Check back later.", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "Ok", style: .default))
            self.present(alert, animated: true)
        }
    }
    
    func schoolsWereUpdated(schools: [School]?) {
        DispatchQueue.main.async {
            self.schools = schools
        }
    }
}
