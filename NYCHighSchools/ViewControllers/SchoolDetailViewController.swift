//
//  SchoolDetailViewController.swift
//  NYCHighSchools
//
//  Created by Alex Elkins on 7/19/19.
//  Copyright © 2019 Alex Elkins. All rights reserved.
//

import UIKit

class SchoolDetailViewController: UIViewController {
    var school: School? = nil
    
    @IBOutlet var schoolName: UILabel!
    @IBOutlet var addressLine1: UILabel!
    @IBOutlet var city: UILabel!
    @IBOutlet var zip: UILabel!
    @IBOutlet var phoneNumber: UIButton!
    @IBOutlet var tableView: UITableView!
    
    var satDataService: SATDataService?
    var satResult: SATResult? {
        didSet {
            DispatchQueue.main.async {
                self.updateUI()
            }
        }
    }
    
    func updateUI() {
        self.schoolName.text = school?.schoolName
        self.addressLine1.text = school?.addressLine1
        self.city.text = school?.city
        self.zip.text = school?.zip
        self.phoneNumber.setTitle(school?.phoneNumber, for: .normal)
        tableView.reloadData()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.navigationBar.prefersLargeTitles = false
        self.satDataService = SATDataService(satDataServiceDelegate: self)
        if let school = school {
            self.satDataService?.getSatResultFor(school: school)
        }
        updateUI()
    }
    @IBAction func phoneNumberWasPressed(_ sender: UIButton) {
        guard let phoneNumber = school?.phoneNumber, let url = URL(string: "telprompt://\(phoneNumber)") else {
            fatalError("Failed to generate phone number for school!")
        }
        UIApplication.shared.open(url, options: [:], completionHandler: nil)
    }
}

extension SchoolDetailViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 4
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "SATResultCell", for: indexPath)
        switch indexPath.row {
        case 0:
            cell.textLabel?.text = "Number of SAT Test Takers"
            cell.detailTextLabel?.text = satResult?.numberOfTestTakers
            break
        case 1:
            cell.textLabel?.text = "Critical Reading Average Score"
            cell.detailTextLabel?.text = satResult?.criticalReadingAverageScore
            break
        case 2:
            cell.textLabel?.text = "Math Average Score"
            cell.detailTextLabel?.text = satResult?.mathAverageScore
            break
        case 3:
            cell.textLabel?.text = "Writing Average Score"
            cell.detailTextLabel?.text = satResult?.writingAverageScore
            break
        default:
            break
        }
        return cell
    }
}

extension SchoolDetailViewController: SATDataServiceDelegate {
    func satResultWasUpdated(satResult: SATResult?) {
        self.satResult = satResult
    }
    
    
}
