//
//  NYCHighSchoolsTests.swift
//  NYCHighSchoolsTests
//
//  Created by Alex Elkins on 7/19/19.
//  Copyright © 2019 Alex Elkins. All rights reserved.
//

import XCTest
@testable import NYCHighSchools

class NYCHighSchoolsTests: XCTestCase {

    var networkClient: NetworkClient!
    let session = URLSessionMock()
    
    override func setUp() {
        super.setUp()
        networkClient = NetworkClient(session: session)
    }
    
    func testNetworkClientAcceptsGets() {
        guard let url = URL(string: networkClient.schoolDataEndpoint) else {
            fatalError("Failed to generate URL from JSON endpoint")
        }
        networkClient.get(url: url) { (data, error) in
            
        }
        XCTAssert(session.lastUrl == url)
    }
    
    func testNetworkClientCallsResume() {
        let dataTask = URLSessionMockDataTask()
        session.nextDataTask = dataTask
        guard let url = URL(string: networkClient.schoolDataEndpoint) else {
            fatalError("Failed to generate URL from JSON endpoint")
        }
        networkClient.get(url: url) { (data, error) in
            
        }
        XCTAssert(dataTask.resumeWasCalled)
    }
    
    func testNetworkClientGetsData() {
        let expectedData = "{}".data(using: .utf8)
        session.nextData = expectedData
        var actualData: Data?
        guard let url = URL(string: networkClient.schoolDataEndpoint) else {
            fatalError("Failed to generate URL from JSON endpoint")
        }
        networkClient.get(url: url) { (data, error) in
            actualData = data
        }
        XCTAssertNotNil(actualData)
    }
}
